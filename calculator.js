const fs = require('fs');
const constants = require('./constants.js')

function suma(num1, num2){
    let result = num1 + num2; 
    mensaje(num1, num2, '+', result);
    return result;
}

function resta(num1, num2) {
    let result = num1 - num2;
    mensaje(num1, num2, '-', result);
    return result;
}

function multiplicar(num1, num2) {
    let result = num1 * num2;
    mensaje(num1, num2, '*', result);
    return result;
}

function dividir(num1, num2) {
    let result = num1 / num2;
    mensaje(num1, num2, '/', result);
    return result;
}

function mensaje(num1, num2, op, result) {
    let string = `${num1} ${op} ${num2} = ${result} \n`;
    fs.writeFileSync(constants.logFile, string, {flag: 'a'});
}

module.exports = {
    'suma': suma,
    'resta': resta,
    'multiplicar': multiplicar,
    'dividir': dividir
}